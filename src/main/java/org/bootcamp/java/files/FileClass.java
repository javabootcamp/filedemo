package org.bootcamp.java.files;

import org.apache.commons.io.FileUtils;
import org.bootcamp.java.apache.CommonsLib;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class FileClass {


    public static void main(String[] args) throws Exception {

        String readFilePath = "src/main/resources/data/in/someFile.json";
        String readRelativePath = "data/in/someFile.json";
        
        String writeFilePath = "src/main/resources/data/out/someFile.json";


        //READ
        String fileContents0 = readFileContentsFullPath(readFilePath);
        System.out.println(fileContents0);

        String fileContents1 = readFileContentsFromResources(readRelativePath);
        System.out.println(fileContents1);

        //WRITE
        writeFile(writeFilePath,fileContents0);

    }

    public static String readFileContentsFullPath(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        StringBuilder content = new StringBuilder();

        List<String> lines = Files.readAllLines(path);
        for (String s : lines) {
            content.append(s);
        }

        return content.toString();
    }

    public static String readFileContentsFromResources(String fileName) throws IOException {
        ClassLoader classLoader = FileClass.class.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        StringBuilder content = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(file.toURI()));
        for (String s : lines) {
            content.append(s);
        }

        return content.toString();
    }


    public static void writeFile(String fileName, String content) throws IOException {
        Path path = Paths.get(fileName);
        List<String> lines = Arrays.asList(content.split("\n"));
        Files.write(path,lines);
    }
}
