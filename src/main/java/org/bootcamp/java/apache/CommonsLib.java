package org.bootcamp.java.apache;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class CommonsLib {

    public static void main(String[] args) throws Exception {

        String readFilePath = "src/main/resources/data/in/someFile.json";
        String writeFilePath = "src/main/resources/data/out/someFileApache.json";
        String readRelativePath = "data/in/someFile.json";

        //READ
        String fileContents0 = readFileContentsFullPath(readFilePath);
        System.out.println(fileContents0);

        String fileContents1 = readFileContentsFromResources(readRelativePath);
        System.out.println(fileContents1);

        //WRITE
        writeFile(writeFilePath,fileContents0);

    }

    public static String readFileContentsFullPath(String fileName) throws IOException {
        File file = new File(fileName);
        String content = FileUtils.readFileToString(file,"UTF-8");
        return content;
    }

    public static String readFileContentsFromResources(String fileName) throws IOException {
        ClassLoader classLoader = CommonsLib.class.getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());
        String content = FileUtils.readFileToString(file,"UTF-8");
        return content;
    }

    public static void writeFile(String fileName, String content) throws IOException {
        File file = new File(fileName);
        FileUtils.writeStringToFile(file,content,"UTF-8");
    }
}
